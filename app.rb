require 'rubygems'
require 'date'
require 'sinatra/base'
require 'json'
require 'em-websocket'
require 'em-hiredis'
require 'redis'
require_relative 'app/constants'
require_relative 'app/init'
require_relative 'app/model/init'

#channel method creation for single push
module EventMachine
  class Channel
    def pushOne(*items)
      puts "****** pushOne *****"
      puts "@subs : #{@subs}"
      items = items.dup
      EM.schedule { items.each { |i| [@subs.values.first].each { |s| s.call i } } }
    end
  end
end

# ref. http://stackoverflow.com/questions/20642717/how-to-run-eventmachine-and-serve-pages-in-sinatra
# ref. https://gist.github.com/gvarela/957367
def run(opts)
  EM.run do
    server  = opts[:server] || 'thin'
    host    = opts[:host]   || '0.0.0.0'
    port    = opts[:port]   || ENV["CHAT_ASSIST_PORT"] || '8083'
    web_app = opts[:app]


    dispatch = Rack::Builder.app do
      map '/' do
        run web_app
      end
    end

    unless ['thin', 'hatetepe', 'goliath'].include? server
      raise "Need an EM webserver, but #{server} isn't"
    end

    Rack::Server.start({
       app:    dispatch,
       server: server,
       Host:   host,
       Port:   port,
       signals: false,
    })



    puts "chat server starting"
    @channel = EM::Channel.new

    @rooms = {}
    @sids = {}

    #@redis = EM::Hiredis.connect("redis://localhost:6379").pubsub
    @redis = EM::Hiredis.connect("redis://127.0.0.1:6379").pubsub
    puts 'subscribing to redis'
    @redis.subscribe("ws")
    @redis.on(:message){|channel, message|
      puts "redis -> #{channel}: #{message[0, 100]}"
      #@channel.push message
      @channel.pushOne message
    }

    # Creates a websocket listener
    EventMachine::WebSocket.start(:host => '0.0.0.0', :port => 8084) do |ws|
      puts 'Establishing websocket'
      ws.onopen do |handshake|
        roomNumber = handshake.path.split("/")[1]
        puts "client connected. roomNumber: #{roomNumber}"

        if @rooms[roomNumber].nil?
          @rooms[roomNumber] = []
        end
        @rooms[roomNumber] << ws
        puts "rooms: #{@rooms}"

        puts "subscribing to channel"
        sid = @channel.subscribe do |msg|
          msgHash = JSON.parse(msg)
          puts "roomNumber: #{msgHash["roomNumber"]}, sending: #{msg[0, 100]}"
          @rooms[msgHash["roomNumber"]].each do |roomWs|
            roomWs.send msg
          end
        end

        if @sids[roomNumber].nil?
          @sids[roomNumber] = []
        end
        @sids[roomNumber] << sid

        ws.onmessage { |msg|
          @channel.push "<#{sid}>: #{msg}"
        }

        ws.onclose {
          @channel.unsubscribe(sid)
        }
      end
    end

  end
end

class ChatAssistApp < Sinatra::Base

  configure do
    set :threaded, false
    set :show_exceptions, false
    enable :logging
    enable :sessions
    set :session_secret, 'super secret'
    disable :protection
    #set :redisPub, Redis.new(:url => "redis://localhost:6379")
    set :redisPub, Redis.new(:url => "redis://127.0.0.1:6379")
  end

  error do
    e = env['sinatra.error']
    @error = e.message

    if request.xhr?
      {:result => "#{e}", :message => e.message}
    else
      erb :index
    end
  end

  before do
    puts "## sessionId: #{session['session_id']}, #{session[:loginUser]}"
    if request.path_info.start_with?("/admin/")
      if session[:loginUser].nil? or !session[:loginUser][:isAdmin]
        raise "해당 페이지는 관리자만 접근할 수 있습니다."
      end
    elsif request.path_info.start_with?("/rooms/")
      if session[:loginUser].nil?
        raise "해당 페이지는 권한을 받아 로그인한 사용자만 접근할 수 있습니다."
      end
    end
  end

  get '/' do
    dl = DataLoader.new
    @rooms = dl.getAllRooms

    erb :index
  end

  post '/signup' do
    userId = params[:userId]
    password = params[:password]
    name = params[:name]
    email = params[:email]

    dl = DataLoader.new
    dl.addUser(userId, password, name, email)

    erb :index
  end

  post '/login' do
    dl = DataLoader.new
    foundUser = dl.loginUser(params[:userId], params[:password])

    if foundUser.nil?
      raise "로그인에 실패하였습니다."
    else
      session[:loginUser] = foundUser
    end

    @rooms = dl.getAllRooms

    erb :index
  end

  get '/logout' do
    session[:loginUser] = nil

    erb :index
  end

  post '/rooms/chat' do
    dl = DataLoader.new
    @roomNumber = dl.addRoom(params[:roomName])
    erb :chat
  end

  get '/rooms/chat/:roomNumber' do
    @roomNumber = params[:roomNumber]||1
    erb :chat
  end

  get '/rooms/chat/:roomNumber/comments' do
    dl = DataLoader.new
    comments = dl.getComments(params[:roomNumber])
    comments.to_json
  end

  def getChatParams
    commentText = params[:commentText]
    user = session[:loginUser]
    roomNumber = params[:roomNumber]
    puts "room: #{roomNumber}, user: #{user[:name]}, commentText : #{commentText}"
    return commentText, roomNumber, user
  end

  def checkAndProcessCommand(commentText, roomNumber)
    if commentText.start_with?("#")
      st = OPENAPI_11ST.new
      if commentText.start_with?("#search ")
        puts "command detected. 11st searching.."
        keyword = commentText.split(" ")[1]
        products = st.searchProduct(keyword)
        #puts products
        settings.redisPub.publish("ws", {:products => products, :roomNumber => roomNumber}.to_json)
      elsif commentText.start_with?("#searchAli ")
          puts "command detected. aliexpress searching.."
          keyword = commentText.split(" ")[1]
          ali = OPENAPI_ALIEXPRESS.new
          products = ali.searchProduct(keyword)
          settings.redisPub.publish("ws", {:aliProducts => products, :roomNumber => roomNumber}.to_json)
      elsif commentText.start_with?("#searchCoupang ")
        puts "command detected. coupang searching.."
        keyword = commentText.split(" ")[1]
        coupang = OPENAPI_COUPANG.new
        products = coupang.searchProduct(keyword)
        settings.redisPub.publish("ws", {:coupangProducts => products, :roomNumber => roomNumber}.to_json)
      elsif commentText.start_with?("#searchGmarket ")
        puts "command detected. gmarket searching.."
        keyword = commentText.split(" ")[1]
        gmarket = OPENAPI_GMARKET.new
        products = gmarket.searchProduct(keyword)
        settings.redisPub.publish("ws", {:gmarketProducts => products, :roomNumber => roomNumber}.to_json)
      elsif commentText.start_with?("#categories")
        puts "command detected. all categories.."
        categories = st.searchAllCategories
        settings.redisPub.publish("ws", {:categories => categories, :roomNumber => roomNumber}.to_json)
      elsif commentText.start_with?("#category")
        categoryCode = commentText.split(" ")[1]
        puts "command detected. category #{categoryCode}"
        products = st.searchCategory(categoryCode)
        settings.redisPub.publish("ws", {:products => products, :roomNumber => roomNumber}.to_json)
      end
    end
  end

  post '/rooms/chat/:roomNumber/comment' do
    commentText, roomNumber, user = getChatParams

    userName = user[:name]
    userId = user[:userId]
    newComment = { roomNumber: roomNumber, userId: userId, name: userName, createdAt: Time.now, commentText: commentText }

    # publish to redis -> ws push
    settings.redisPub.publish("ws", newComment.to_json)

    # keep data to db
    dl = DataLoader.new
    dl.addComment(newComment)

    # command check
    checkAndProcessCommand(commentText, roomNumber)

    {result: "success"}.to_json
  end

  post '/rooms/chat/:roomNumber/searchAgain' do
    commentText, roomNumber, user = getChatParams

    userName = user[:name]
    userId = user[:userId]

    # command check
    checkAndProcessCommand(commentText, roomNumber)

    {result: "success"}.to_json
  end
end

run app: ChatAssistApp.new


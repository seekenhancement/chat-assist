require 'sequel'
require 'mysql2'
require 'json'
require 'logger'

DB = Sequel.connect(:adapter => 'mysql2', :host => ENV["MYSQL_HOST"]||'localhost', :database => ENV["MYSQL_DATABASE"]||'chat_assist', :user => ENV["MYSQL_USER"]||'assist', :password => ENV["MYSQL_PASSWORD"]||'assist00')
DB.extension :connection_validator
DB.pool.connection_validation_timeout = -1
DB.loggers << Logger.new($stdout)

require_relative '../constants'
require_relative 'data_loader'

require 'erb'

class DataLoader
  include Constants

  def initialize
  end

  def addUser(userId, password, name, email)
    DB[:user].insert(:userId => userId, :password => password, :name => name, :email => email, :isAdmin => false)
  end

  def loginUser(userId, password)
    puts "#{userId} login"
    DB[:user].where(:userId => userId, :password => password).first
  end

  def getAllRooms
    DB[:rooms].all
  end

  def addRoom(roomName)
    DB[:rooms].insert(:name => roomName)
  end

  def getComments(roomNumber)
    DB[:chat].join(:user, :userId => :userId).where(:roomNumber => roomNumber).order(:id).all
  end

  def addComment(comment)
    DB[:chat].insert(:roomNumber => comment[:roomNumber], :userId => comment[:userId], :commentText => comment[:commentText], :createdAt => comment[:createdAt])
  end

end

require 'rest-client'
require 'uri'
require 'nokogiri'


class OPENAPI_GMARKET
  include Constants

  OPENAPI_GMARKET_URL = "http://mobile.gmarket.co.kr"
  PRODUCT_SEARCH = "/Search/SearchJson"

  def initialize
    # auth token
  end

  def searchProduct(searchKeyword)
    puts "gmarket #{searchKeyword} searching started"
=begin
    result = RestClient.get("#{OPENAPI_GMARKET_URL}#{PRODUCT_SEARCH}#{URI::encode(searchKeyword)}").body
    page = Nokogiri::HTML(result)
    products = []
    page.css('#content div.mw_wrap div.mw_contbx div.kind_artwrap ul.best_lst li').each do |l|
      detailUrl = OPENAPI_COUPANG_URL + l.css("a").attr("href")
      imageUrl = l.css("a span.img_box img").attr("src").value
      productName = l.css("a span.info_box strong.tit_pd").text().strip
      salePrice = l.css("a span.info_box span.pr_info strong.pd_price em.cp").text().strip
      orgPrice = l.css("a span.info_box span.pr_info strong.pd_price span.ori_price").text().strip
      saleRate = l.css("a span.info_box span.pr_info span.sale").text().strip
      delivery = l.css("a span.info_delivery span").text().strip
      products << { :detailUrl => detailUrl, :imageUrl => imageUrl, :productName => productName, :salePrice => salePrice, :orgPrice => orgPrice, :saleRate => saleRate, :delivery => delivery }
    end
    products
=end
    result = JSON.parse(RestClient.post("#{OPENAPI_GMARKET_URL}#{PRODUCT_SEARCH}", { "primeKeyword" => searchKeyword}.to_json, content_type: :json))
    products = result["Item"].first(20)
  end

end

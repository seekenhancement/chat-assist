require 'rest-client'
require 'uri'
require 'nokogiri'


class OPENAPI_COUPANG
  include Constants

  OPENAPI_COUPANG_URL = "http://m.coupang.com"
  PRODUCT_SEARCH = "/nm/search?q="

  def initialize
    # auth token
  end

  def searchProduct(searchKeyword)
    puts "coupang #{searchKeyword} searching started"
    result = RestClient.get("#{OPENAPI_COUPANG_URL}#{PRODUCT_SEARCH}#{URI::encode(searchKeyword)}").body
    page = Nokogiri::HTML(result)
    products = []
    page.css('#goodsList ul li').each do |l|
      detailUrl = OPENAPI_COUPANG_URL + l.css("a").attr("href")
      imageUrl = l.css("a div.plp-content span.plp-img img").attr("src").value
      productName = l.css("a div.plp-content span.plp-info div.title").text()
      salePrice = l.css("a div.plp-content span.plp-info div.plp-info-price span.sale-price").text().strip
      delivery = l.css("a div.plp-footer span.plp-badge span.badge-shipping").text().strip
      products << { :detailUrl => detailUrl, :imageUrl => imageUrl, :productName => productName, :salePrice => salePrice, :delivery => delivery }
    end
    products
  end

end

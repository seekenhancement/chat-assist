Chat Assist
===========

관심 주제(현재는 쇼핑) 별 live chat room 을 개설하고 chat room 내에서는 
11번가 Open API (카테고리/상품검색) 를 미리 정의한 간략한 명령어로 chat 화면상에서 입력 및 결과 조회가 가능한 시스템을 구현한다.

구현 기술
=========

. ruby 2.2.5, sinatra, websocket-eventmachine-server
. bootstrap, angularjs, angular-websocket
. mongodb ?

. 11st Open API (https://atmos.skplanet.com/products/11st/apis/b75813c3/branches/release/revisions/latest/spec)




require 'logger'

module Constants
  OPENAPI_11ST_APPKEY = ENV["OPENAPI_11ST_APPKEY"]||'dummy_app_key'

  @@global_keep = {}

  def dl
    @dl = DataLoader.new if @dl.nil?
    @dl
  end
end

require 'rest-client'
require 'uri'


class OPENAPI_11ST
  include Constants

  OPENAPI_11ST_URL = "https://apis.skplanetx.com"
  CATEGORY_SEARCH = "/11st/v2/common/categories"
  PRODUCT_SEARCH = "/11st/v2/common/products"

  def initialize
    # auth token
  end

  def searchAllCategories
    puts "searching all categories"
    result = JSON.parse(RestClient.get("#{OPENAPI_11ST_URL}#{CATEGORY_SEARCH}", {:Accept => "application/json", :appKey => OPENAPI_11ST_APPKEY}))
    categories = result["CategoryResponse"]["Children"]["Category"]
    categories
  end

  def searchCategory(categoryCode)
    puts "#{categoryCode} searching category"
    result = JSON.parse(RestClient.get("#{OPENAPI_11ST_URL}#{CATEGORY_SEARCH}/#{categoryCode}?option=Products:", {:Accept => "application/json", :appKey => OPENAPI_11ST_APPKEY}))
    categoryProducts = result["CategoryResponse"]["Products"]["Product"]
    categoryProducts.first(10)
  end

  def searchProduct(searchKeyword)
    puts "#{searchKeyword} searching started"
    result = JSON.parse(RestClient.get("#{OPENAPI_11ST_URL}#{PRODUCT_SEARCH}?searchKeyword=#{URI::encode(searchKeyword)}", {:Accept => "application/json", :appKey => OPENAPI_11ST_APPKEY}))
    products = result["ProductSearchResponse"]["Products"]
    #totalCount = products["TotalCount"]
    products["Product"].first(10)
  end

end

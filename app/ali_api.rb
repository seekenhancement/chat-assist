require 'rest-client'
require 'uri'


class OPENAPI_ALIEXPRESS
  include Constants

  OPENAPI_ALIEXPRESS_URL = "https://m.aliexpress.com"
  PRODUCT_SEARCH = "/search/productListJson.htm?keywords=**SEARCH_KEYWORD**&sortType=MAIN&shippingCountry=KR&viewtype=1&version=4|2|3"

  def initialize
    # auth token
  end

  def searchProduct(searchKeyword)
    puts "aliexpress #{searchKeyword} searching started"
    result = JSON.parse(RestClient.get("#{OPENAPI_ALIEXPRESS_URL}#{PRODUCT_SEARCH.gsub("**SEARCH_KEYWORD**", URI::encode(searchKeyword))}", {:Accept => "application/json" }))
    result["productList"]
  end

end

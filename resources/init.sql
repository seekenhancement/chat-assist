DROP TABLE Chat;
DROP TABLE Rooms;
DROP TABLE User;

CREATE TABLE User (
  userId VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  name VARCHAR(50) NOT NULL,
  email VARCHAR(100),
  isAdmin BOOLEAN,
  CONSTRAINT user_pk PRIMARY KEY (userId)
);

CREATE TABLE Rooms (
  roomNumber INTEGER NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  CONSTRAINT rooms_pk PRIMARY KEY (roomNumber)
);

CREATE TABLE Chat (
  id INTEGER NOT NULL AUTO_INCREMENT,
  roomNumber INTEGER NOT NULL,
  userId VARCHAR(50) NOT NULL,
  commentText VARCHAR(4000) NOT NULL,
  createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT chat_pk PRIMARY KEY (id),
  CONSTRAINT chat_roomNumber_fk FOREIGN KEY (roomNumber) REFERENCES Rooms(roomNumber)
);

insert into User(userId, password, name, email, isAdmin) values ('woo', 'woo00', '우병훈', 'byunghun.woo@sk.com', true);
insert into User(userId, password, name, email, isAdmin) values ('seek', 'seek00', '똘이아빠', 'seekenhancement@gmail.com', false);

insert into Rooms(roomNumber, name) values (1, '11번가 상품talk');